#= require jquery
#= require handlebars
#= require ember
#= require ember-data
#= require_self
#= require ozinbo

# for more details see: http://emberjs.com/guides/application/
window.Ozinbo = Ember.Application.create()

