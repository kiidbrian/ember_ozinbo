# For more information see: http://emberjs.com/guides/routing/

Ozinbo.Router.map ()->
  @resource('transactions')
  @resource('payments')
  @resource('refunds')
  @resource('accounts')
  @resource('messages')